"use strict"

const clients1 = ["Гилберт", "Сальваторе", "Пирс", "Соммерс", "Форбс", "Донован", "Беннет"];
const clients2 = ["Пирс", "Зальцман", "Сальваторе", "Майклсон"];

const allClients = [...new Set([...clients1, ...clients2])];

//деструктуризация - это присвоение ключей объектов или элемнтов массивов в отдельные переменные
//нужно для более удобной работы с большими объектами или массивами, когда нужно выделять несколько или только часть значений
//короткий, удобный синаксис без методов, циклов и прочего